# cryptowatch-bot

pulls & analyses data from https://cryptowat.ch, figures out what to do and pings you on Slack

![alt text](img/cwbot.png "how the message looks like")

## why

crypto trading needs more automation. at this time this bot is pretty stupid, but it has potential.

## how it works

this bot polls the https://cryptowat.ch rest api to get the latest selected crypto prices. then, the bot calculates "simple moving average" crossover and if it finds a trade signal, it will ping my Slack, telling me when it triggered and what should I do.


## how to deploy

there are kustomized deployment manifests for k8s  
just add your Slack webhook url into the Secret