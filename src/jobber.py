#TODO add retry timeout and retry count, alerting
#TODO add checking for quiet time
#TODO add storing state locally (in a file?)
#TODO add tests
#TODO add profitability backtests
#TODO add integration for automated trading at BINANCE

# import ipdb; ipdb.set_trace()

import sys, yaml, traceback, requests
import logging as log
from  os import environ as envvar
import datetime
import time
import requests
import pickle
import boto3


import pandas as pd

from pprint import pprint as pp

import cryptowatch as cw

log.basicConfig(format='jobber: %(levelname)s:%(message)s', level=log.ERROR)

def download_s3_obj(s3_path, local_path):

    bucket_name = s3_path.split("/")[2]
    object_path = "/".join(s3_path.split("/")[3:len(s3_path.split("/"))])

    s3 = boto3.resource("s3")
    bucket = s3.Bucket(bucket_name)
    
    bucket.download_file(object_path, local_path)

def upload_s3_obj(local_path, s3_path):

    bucket_name = s3_path.split("/")[2]
    object_path = "/".join(s3_path.split("/")[3:len(s3_path.split("/"))])

    s3_client = boto3.client("s3")

    s3_client.upload_file(local_path, bucket_name, object_path)


DEFAULT_LOOKBACK_TIME_SEC = 14400*6*7*3


signals_mapping = {
    "-1": {
        "string":"sell",
        "icon":":red_circle:"
        },
    "1": {
        "string":"buy",
        "icon":":large_green_circle:"
        },
    "0": {
        "string":"wait",
        "icon":""
        }
}

# load envvars

try:
    STATE_PATH      = envvar['STATE_PATH']
    CONFIG_PATH     = envvar['CONFIG_PATH']
except Exception:
    log.debug(traceback.print_exc())
    log.error("basic envvar read failed")
    sys.exit(1)

try:
    S3_CONFIG_PATH  = envvar['S3_CONFIG_PATH']
    S3_STATE_PATH  = envvar['S3_STATE_PATH']
except Exception:
    log.debug(traceback.print_exc())
    log.error("s3 envvar read failed")


if len(S3_CONFIG_PATH) > 0:
    log.info("loading config from S3")
    try:
        download_s3_obj(s3_path=S3_CONFIG_PATH, local_path=CONFIG_PATH)
        log.info("loaded config from S3")
    except Exception:
        log.error("loading config from S3 failed")


try:
    with open(CONFIG_PATH, "r") as bot_cfg:
        config = yaml.load(bot_cfg, Loader=yaml.FullLoader)
        log.info("config read successful")
except:
    log.error("config read failed")
    traceback.print_exc()
    sys.exit(1)


# load constants from config
try:
    MARKETS         = config['markets']
    EXCHANGE        = config['exchange']
    PERIOD          = config['period']
    STRATEGY        = config['strategy']
    ALERTING        = config['alerting']

except Exception:
    log.error('invalid config?')
    traceback.print_exc()
    exit(1)

log.info("{} cryptowat.ch jobber run {}".format(50 * "#", 50 * "#"))
log.info(pp(config))

def crossover(series_1, series_2):
    if series_1.iloc[-1] > series_2.iloc[-1] \
    and series_1.iloc[-2] <= series_2.iloc[-2]:
        return True
    else:
        return False

def get_data(exchange, markets, period):
    ohlc_data = {}
    for market in markets:
        log.debug("retrieving {} {}".format(exchange,market))
        try:
            ohlc = cw.markets.get(
                "{}:{}".format(exchange, market),
                ohlc=True,
                periods=[period]
                )

            ohlc_df = pd.DataFrame(
                getattr(ohlc, "of_{}".format(period)), 
                columns=[
                    "timestamp",
                    "Open",
                    "High",
                    "Low",
                    "Close",
                    "Volume",
                    "VolumeQuote"
                    ]
                )

            ohlc_df.set_index(ohlc_df["timestamp"], inplace=True) 

            # add indicators per line, todo: smarter loading for this, 
            # adding simple and exponential moving averages

            ohlc_df['SMA_1'] = ohlc_df.Close.rolling(window=STRATEGY["indicators"]["sma_1"]).mean()
            ohlc_df['SMA_2'] = ohlc_df.Close.rolling(window=STRATEGY["indicators"]["sma_2"]).mean()
            
            ohlc_df['EMA_1'] = ohlc_df.Close.ewm(span=STRATEGY["indicators"]["ema_1"],adjust=False).mean()
            ohlc_df['EMA_2'] = ohlc_df.Close.ewm(span=STRATEGY["indicators"]["ema_2"],adjust=False).mean()

            ohlc_data["{}:{}".format(exchange,market)] = ohlc_df


        except Exception:
            
            traceback.print_exc()
            log.error("problem retrieving data")
            sys.exit(1)

        print(ohlc_df.tail(5))
        print("latest record in response: {}".format(ohlc_df.index[-1]))
    return ohlc_data, ohlc._fetched_at.timestamp(), ohlc._allowance

# scan for trade signals, in case of MA strategies, 
# it's crossover of series, 
# todo: smarter loading for this
def moving_avg_strategy(ma1, ma2):
    
    trade_signals = []

    for i in range(1, len(ma1)+1):

        if crossover(ma1.head(i),ma2.head(i)):
            trade_signals.append(1) # buy
        elif crossover(ma2.head(i),ma1.head(i)):
            trade_signals.append(-1) # sell
        else:
            trade_signals.append(0) # wait
    
    return trade_signals


def get_ma_trade_signals(ohlc_data): 
    # takes in ohlc_data, dictionary with "exch:symbol" as key, dataframe as value
    
    # ma_trade_signals = {}

    for exchange_market, ohlc_df in ohlc_data.items():
        print(exchange_market)
        ohlc_df["sma_trade_signal"] = \
            moving_avg_strategy(ohlc_df.SMA_1, ohlc_df.SMA_2)
        ohlc_df["ema_trade_signal"] = \
            moving_avg_strategy(ohlc_df.EMA_1, ohlc_df.EMA_2)

        

        # sma_signal = ohlc_df.sma_trade_signal
        # ema_signal = ohlc_df.ema_trade_signal


        # sma_signal_clear = sma_signal.where(sma_signal != 0).dropna()
        # ema_signal_clear = ema_signal.where(ema_signal != 0).dropna()

        # ma_trade_signals[exchange_market] = {
        #     "sma": sma_signal_clear,
        #     "ema": ema_signal_clear,
        #     "sma_full": sma_signal,
        #     "ema_full": ema_signal
        # }

    return ohlc_data

def save_state(state, state_path):
    '''
    state should contain:
        - dataframe with ohlc and trade signals
        - latest successful alert timestamp
    '''
    with open(state_path, "wb") as state_file:
        pickle.dump(state,state_file, protocol=pickle.HIGHEST_PROTOCOL)

def load_state(state_path):

    with open(state_path, "rb") as state_file:
        state = pickle.load(state_file)
    
    return state

# Message building functions

def get_alert_data(current_ohlc_data, stored_ohlc_data):

    '''
    compare the old dataframe to the current,
    are there new trade signals on either trade signal series? stage them to be sent to slack!
    '''

    alerts = {}

    for exchange_market, ohlc_df in current_ohlc_data.items():
        
        current_df = ohlc_df
        stored_df = stored_ohlc_data[exchange_market]

        diff = pd.concat([current_df, stored_df]).drop_duplicates(keep=False)
        alerts[exchange_market] = diff[
            (diff.sma_trade_signal != 0) | (diff.ema_trade_signal != 0)
            ].dropna()[
                ["sma_trade_signal","ema_trade_signal"]
            ].to_dict()
        

    # import ipdb; ipdb.set_trace()



    return alerts


def create_message_attachments(alerts):
    

    attachments = []
    
    for exchange_market, alert_data in alerts.items():
        
        exchange, market = exchange_market.split(":")
        
        for alert_type, alert_datapoints in alert_data.items():

            indicator = alert_type.split("_")[0]

            for ts, value in alert_datapoints.items():

                icon = signals_mapping[str(value)]["icon"]
                string = signals_mapping[str(value)]["string"]
                if (time.time() - ts) > 0:
                    time_delta = str(datetime.timedelta(seconds = round(time.time() - ts))) + "ago"
                else:
                    time_delta = "right now!"

                if value == 0:
                    continue

                attachments.append({
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": "{} {} {} *{}* {} _{}_".format(
                            market.upper(),
                            indicator,
                            icon,
                            string,
                            exchange,
                            time_delta

                        )
                    }
                }
                )

    return attachments


def send_slack_message(slack_webhook, message_payload): 

    #TODO: remove dependency on config, should be loaded more transparently?

    slack_message = {
        "text": "exch: {}, markets: _{}_".format(
            EXCHANGE.upper(),
            "|".join(MARKETS).upper()
            ),
        "username": "jobber-bot",
        "channel": "trading",
        "icon_emoji": ":hedonism-bot:",
        "blocks": message_payload
    }

    
    r = requests.post(slack_webhook, json=slack_message)
    
    print(slack_message)
    print(r.status_code)

def init():




    if len(S3_STATE_PATH) > 0:
        try:
            download_s3_obj(S3_STATE_PATH, STATE_PATH)
        except Exception:
            log.info("no S3 state loaded")


    try:
        state = load_state(STATE_PATH)
        log.info("found existing state")
    except Exception:

        INIT_MESSAGE = [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":wave: hello there, this is cryptowatch jobber bot! just initialised with following config: ```{}```".format(config)
                        }
                    }
                    ]
                    
        LOOKBACK_ROWS = 50

        # get initial data and save without last X rows (LOOKBACK_ROWS constant)
        # this way it'll alert the latest alerts and ensure existence of state
        ohlc_data, _ , _ = get_data(exchange=EXCHANGE, markets=MARKETS, period=PERIOD)
        ohlc_data_mats = get_ma_trade_signals(ohlc_data) #add trade signals to the dataframes
        
        for exchange_market, ohlc_df in ohlc_data_mats.items():
            ohlc_data_mats[exchange_market] = ohlc_df.head(
                len(ohlc_df.index) - LOOKBACK_ROWS
            )
            
        save_state(ohlc_data_mats, STATE_PATH)
        log.info("state initialised")
        send_slack_message(
            slack_webhook= ALERTING["slack_webhook"], 
            message_payload=INIT_MESSAGE
            )
        


def main():


    # while True:

    init()

    state = load_state(STATE_PATH)

    ohlc_data, _ , _ = get_data(exchange=EXCHANGE, markets=MARKETS, period=PERIOD)
    
    
    ohlc_data_mats = get_ma_trade_signals(ohlc_data) #add trade signals to the dataframes


    alerts = get_alert_data(current_ohlc_data=ohlc_data, stored_ohlc_data=state)
    # import ipdb; ipdb.set_trace()
    
    attachments = create_message_attachments(alerts)

    if len(attachments) > 0:
        send_slack_message(
            slack_webhook= ALERTING["slack_webhook"], 
            message_payload=attachments
            )

    # import ipdb; ipdb.set_trace()



    save_state(ohlc_data_mats, STATE_PATH)

    if len(S3_STATE_PATH) > 0:
        try:
            upload_s3_obj(STATE_PATH, S3_STATE_PATH)
        except Exception:
            log.error("uploading state to S3 failed")

if __name__ == "__main__":
    main()
else:
    pass